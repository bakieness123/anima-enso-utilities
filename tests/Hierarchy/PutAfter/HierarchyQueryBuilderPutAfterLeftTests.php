<?php

namespace Yadda\Enso\Utilities\Tests\Hierarchy\PutAfter;

use Yadda\Enso\Utilities\Tests\TestCase;

/**
 * Tests that a node can be 'putUnder' a node to the left of it's current
 * position in the hierarchy tree, testing various constraints and conditions
 *
 * @return void
 */
class HierarchyQueryBuilderPutAfterLeftTests extends TestCase
{
    // /**
    //  * Tests that a node with no children can be moved left to be a child of a
    //  * node with no children
    //  *
    //  * @return void
    //  */
    // public function test_the_put_after_functionality_moving_left_with_no_children_and_no_sibling_children()
    // {
    //     $this->generateComplexNodeTree();

    //     // Move a node with no children under another node with no children
    //     $this->assertTrue($this->nodes[14]->putAfter($this->nodes[9]));

    //     // Reload the tree so to make checks that the appropriate nodes around
    //     // and between the change sites are correct
    //     $this->refreshTree();

    //     // Check moved node is correct
    //     $this->assertEquals($this->nodes[14]->getHierarchyParentId(), 8);
    //     $this->assertEquals($this->nodes[14]->getHierarchyLeftId(), 19);
    //     $this->assertEquals($this->nodes[14]->getHierarchyRightId(), 20);
    //     $this->assertEquals($this->nodes[14]->getHierarchyDepth(), 3);

    //     // Check node to the left of change sites is unchanged.
    //     $this->assertEquals($this->nodes[1]->getHierarchyLeftId(), 2);
    //     $this->assertEquals($this->nodes[1]->getHierarchyRightId(), 3);

    //     // Check root right id updated but not left id
    //     $this->assertEquals($this->nodes[6]->getHierarchyLeftId(), 13);
    //     $this->assertEquals($this->nodes[6]->getHierarchyRightId(), 24);

    //     // Check parent node right id updated but not left_id
    //     $this->assertEquals($this->nodes[7]->getHierarchyLeftId(), 14);
    //     $this->assertEquals($this->nodes[7]->getHierarchyRightId(), 21);

    //     // Check node fully between change sites updated
    //     $this->assertEquals($this->nodes[10]->getHierarchyLeftId(), 22);
    //     $this->assertEquals($this->nodes[10]->getHierarchyRightId(), 23);

    //     // Check root of move FROM node is update left id but not right
    //     $this->assertEquals($this->nodes[11]->getHierarchyLeftId(), 25);
    //     $this->assertEquals($this->nodes[11]->getHierarchyRightId(), 34);

    //     // Check parent of move FROM node is update left id but not right
    //     $this->assertEquals($this->nodes[13]->getHierarchyLeftId(), 27);
    //     $this->assertEquals($this->nodes[13]->getHierarchyRightId(), 30);

    //     // Check node to the right of change sites is unchanged
    //     $this->assertEquals($this->nodes[16]->getHierarchyLeftId(), 32);
    //     $this->assertEquals($this->nodes[16]->getHierarchyRightId(), 33);
    // }

    // /**
    //  * Tests that a node with children can be moved left to be a child of a
    //  * node with no children
    //  *
    //  * @return void
    //  */
    // public function test_the_put_after_functionality_moving_left_with_children_and_no_sibling_children()
    // {
    //     $this->generateComplexNodeTree();

    //     // Move a node with chilren under a node with no children.
    //     $this->assertTrue($this->nodes[13]->putAfter($this->nodes[9]));

    //     // Reload the tree so to make checks that the appropriate nodes around
    //     // and between the change sites are correct
    //     $this->refreshTree();

    //     // Check moved node is correct
    //     $this->assertEquals($this->nodes[13]->getHierarchyParentId(), 8);
    //     $this->assertEquals($this->nodes[13]->getHierarchyLeftId(), 19);
    //     $this->assertEquals($this->nodes[13]->getHierarchyRightId(), 24);
    //     $this->assertEquals($this->nodes[13]->getHierarchyDepth(), 3);

    //     // Check Child of moved node is correct
    //     $this->assertEquals($this->nodes[14]->getHierarchyParentId(), 14);
    //     $this->assertEquals($this->nodes[14]->getHierarchyLeftId(), 20);
    //     $this->assertEquals($this->nodes[14]->getHierarchyRightId(), 21);
    //     $this->assertEquals($this->nodes[14]->getHierarchyDepth(), 4);

    //     // Check node to the left of change sites is unchanged.
    //     $this->assertEquals($this->nodes[1]->getHierarchyLeftId(), 2);
    //     $this->assertEquals($this->nodes[1]->getHierarchyRightId(), 3);

    //     // Check root right id updated but not left id
    //     $this->assertEquals($this->nodes[6]->getHierarchyLeftId(), 13);
    //     $this->assertEquals($this->nodes[6]->getHierarchyRightId(), 28);

    //     // Check parent node right id updated but not left_id
    //     $this->assertEquals($this->nodes[7]->getHierarchyLeftId(), 14);
    //     $this->assertEquals($this->nodes[7]->getHierarchyRightId(), 25);

    //     // Check node fully between change sites updated
    //     $this->assertEquals($this->nodes[10]->getHierarchyLeftId(), 26);
    //     $this->assertEquals($this->nodes[10]->getHierarchyRightId(), 27);

    //     // Check root of move FROM node is update left id but not right
    //     $this->assertEquals($this->nodes[11]->getHierarchyLeftId(), 29);
    //     $this->assertEquals($this->nodes[11]->getHierarchyRightId(), 34);

    //     // Check parent of move FROM node is update left id but not right
    //     $this->assertEquals($this->nodes[12]->getHierarchyLeftId(), 30);
    //     $this->assertEquals($this->nodes[12]->getHierarchyRightId(), 31);

    //     // Check node to the right of change sites is unchanged
    //     $this->assertEquals($this->nodes[16]->getHierarchyLeftId(), 32);
    //     $this->assertEquals($this->nodes[16]->getHierarchyRightId(), 33);
    // }

    // /**
    //  * Tests that a node with no children can be moved left to be a child of a
    //  * node with children
    //  *
    //  * @return void
    //  */
    // public function test_the_put_after_functionality_moving_left_with_no_children_and_sibling_children()
    // {
    //     $this->generateComplexNodeTree();

    //     // Move a node with no children under another node with no children
    //     $this->assertTrue($this->nodes[14]->putAfter($this->nodes[3]));

    //     // Reload the tree so to make checks that the appropriate nodes around
    //     // and between the change sites are correct
    //     $this->refreshTree();

    //     // Check moved node is correct
    //     $this->assertEquals($this->nodes[14]->getHierarchyParentId(), 3);
    //     $this->assertEquals($this->nodes[14]->getHierarchyLeftId(), 11);
    //     $this->assertEquals($this->nodes[14]->getHierarchyRightId(), 12);
    //     $this->assertEquals($this->nodes[14]->getHierarchyDepth(), 3);

    //     // Check node to the left of change sites is unchanged.
    //     $this->assertEquals($this->nodes[1]->getHierarchyLeftId(), 2);
    //     $this->assertEquals($this->nodes[1]->getHierarchyRightId(), 3);

    //     // Check root right id updated but not left id
    //     $this->assertEquals($this->nodes[0]->getHierarchyLeftId(), 1);
    //     $this->assertEquals($this->nodes[0]->getHierarchyRightId(), 14);

    //     // Check parent node right id updated but not left_id
    //     $this->assertEquals($this->nodes[2]->getHierarchyLeftId(), 4);
    //     $this->assertEquals($this->nodes[2]->getHierarchyRightId(), 13);

    //     // Check node fully between change sites updated
    //     $this->assertEquals($this->nodes[7]->getHierarchyLeftId(), 16);
    //     $this->assertEquals($this->nodes[7]->getHierarchyRightId(), 21);

    //     // Check root of move FROM node is update left id but not right
    //     $this->assertEquals($this->nodes[11]->getHierarchyLeftId(), 25);
    //     $this->assertEquals($this->nodes[11]->getHierarchyRightId(), 34);

    //     // Check parent of move FROM node is update left id but not right
    //     $this->assertEquals($this->nodes[13]->getHierarchyLeftId(), 27);
    //     $this->assertEquals($this->nodes[13]->getHierarchyRightId(), 30);

    //     // Check node to the right of change sites is unchanged
    //     $this->assertEquals($this->nodes[16]->getHierarchyLeftId(), 32);
    //     $this->assertEquals($this->nodes[16]->getHierarchyRightId(), 33);
    // }

    // /**
    //  * Tests that a node with children can be moved left to be a child of a
    //  * node with children
    //  *
    //  * @return void
    //  */
    // public function test_the_put_after_functionality_moving_left_with_children_and_parent_children()
    // {
    //     $this->generateComplexNodeTree();

    //     // Move a node with chilren under a node with no children.
    //     $this->assertTrue($this->nodes[13]->putAfter($this->nodes[3]));

    //     // Reload the tree so to make checks that the appropriate nodes around
    //     // and between the change sites are correct
    //     $this->refreshTree();

    //     // Check moved node is correct
    //     $this->assertEquals($this->nodes[13]->getHierarchyParentId(), 3);
    //     $this->assertEquals($this->nodes[13]->getHierarchyLeftId(), 11);
    //     $this->assertEquals($this->nodes[13]->getHierarchyRightId(), 16);
    //     $this->assertEquals($this->nodes[13]->getHierarchyDepth(), 3);

    //     // Check Child of moved node is correct
    //     $this->assertEquals($this->nodes[14]->getHierarchyParentId(), 14);
    //     $this->assertEquals($this->nodes[14]->getHierarchyLeftId(), 12);
    //     $this->assertEquals($this->nodes[14]->getHierarchyRightId(), 13);
    //     $this->assertEquals($this->nodes[14]->getHierarchyDepth(), 4);

    //     // Check node to the left of change sites is unchanged.
    //     $this->assertEquals($this->nodes[1]->getHierarchyLeftId(), 2);
    //     $this->assertEquals($this->nodes[1]->getHierarchyRightId(), 3);

    //     // Check root right id updated but not left id
    //     $this->assertEquals($this->nodes[0]->getHierarchyLeftId(), 1);
    //     $this->assertEquals($this->nodes[0]->getHierarchyRightId(), 18);

    //     // Check parent node right id updated but not left_id
    //     $this->assertEquals($this->nodes[2]->getHierarchyLeftId(), 4);
    //     $this->assertEquals($this->nodes[2]->getHierarchyRightId(), 17);

    //     // Check node fully between change sites updated
    //     $this->assertEquals($this->nodes[7]->getHierarchyLeftId(), 20);
    //     $this->assertEquals($this->nodes[7]->getHierarchyRightId(), 25);

    //     // Check root of move FROM node is update left id but not right
    //     $this->assertEquals($this->nodes[11]->getHierarchyLeftId(), 29);
    //     $this->assertEquals($this->nodes[11]->getHierarchyRightId(), 34);

    //     // Check parent of move FROM node is update left id but not right
    //     $this->assertEquals($this->nodes[12]->getHierarchyLeftId(), 30);
    //     $this->assertEquals($this->nodes[12]->getHierarchyRightId(), 31);

    //     // Check node to the right of change sites is unchanged
    //     $this->assertEquals($this->nodes[16]->getHierarchyLeftId(), 32);
    //     $this->assertEquals($this->nodes[16]->getHierarchyRightId(), 33);
    // }
}
