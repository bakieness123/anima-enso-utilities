<?php

namespace Yadda\Enso\Utilities\Tests\Hierarchy\PutAfter;

use Yadda\Enso\Utilities\Tests\TestCase;

/**
 * Tests that a node can be 'putUnder' a node to the left of it's current
 * position in the hierarchy tree, testing various constraints and conditions
 *
 * @return void
 */
class HierarchyQueryBuilderPutAfterSameTreeTests extends TestCase
{
    // /**
    //  * Tests previously discovered issue with moving an item under another item
    //  * that makes it's new position a sibling of it's previous parent.
    //  *
    //  * @return void
    //  */
    // public function test_put_after_in_same_tree()
    // {
    //     $this->generateSimpleNodeTree();

    //     $this->assertEquals($this->nodes[0]->descendantNodes()->count(), 3);

    //     $this->nodes[3]->putAfter($this->nodes[1]);

    //     $this->refreshTree();

    //     // Root Element should stay the same;
    //     $this->assertNull($this->nodes[0]->getHierarchyParentId());
    //     $this->assertEquals($this->nodes[0]->getHierarchyLeftId(), 1);
    //     $this->assertEquals($this->nodes[0]->getHierarchyRightId(), 8);
    //     $this->assertEquals($this->nodes[0]->getHierarchyDepth(), 1);

    //     // Test Prev Parent
    //     $this->assertEquals($this->nodes[1]->getHierarchyParentId(), 1);
    //     $this->assertEquals($this->nodes[1]->getHierarchyLeftId(), 2);
    //     $this->assertEquals($this->nodes[1]->getHierarchyRightId(), 5);
    //     $this->assertEquals($this->nodes[1]->getHierarchyDepth(), 2);

    //     // Test Prev sibling
    //     $this->assertEquals($this->nodes[2]->getHierarchyParentId(), 2);
    //     $this->assertEquals($this->nodes[2]->getHierarchyLeftId(), 3);
    //     $this->assertEquals($this->nodes[2]->getHierarchyRightId(), 4);
    //     $this->assertEquals($this->nodes[2]->getHierarchyDepth(), 3);

    //     // Test child
    //     $this->assertEquals($this->nodes[3]->getHierarchyParentId(), 1);
    //     $this->assertEquals($this->nodes[3]->getHierarchyLeftId(), 6);
    //     $this->assertEquals($this->nodes[3]->getHierarchyRightId(), 7);
    //     $this->assertEquals($this->nodes[3]->getHierarchyDepth(), 2);
    // }

    // /**
    //  * Tests previously discovered issue with moving an item under another item
    //  * that makes it's new position a sibling of it's previous parent.
    //  *
    //  * @return void
    //  */
    // public function test_put_after_sibling_switch()
    // {
    //     $this->generateSimpleNodeTree();

    //     $this->assertEquals($this->nodes[0]->descendantNodes()->count(), 3);

    //     $this->nodes[2]->putAfter($this->nodes[3]);

    //     $this->refreshTree();

    //     // Root Element should stay the same;
    //     $this->assertNull($this->nodes[0]->getHierarchyParentId());
    //     $this->assertEquals($this->nodes[0]->getHierarchyLeftId(), 1);
    //     $this->assertEquals($this->nodes[0]->getHierarchyRightId(), 8);
    //     $this->assertEquals($this->nodes[0]->getHierarchyDepth(), 1);

    //     // Test Prev Parent
    //     $this->assertEquals($this->nodes[1]->getHierarchyParentId(), 1);
    //     $this->assertEquals($this->nodes[1]->getHierarchyLeftId(), 2);
    //     $this->assertEquals($this->nodes[1]->getHierarchyRightId(), 7);
    //     $this->assertEquals($this->nodes[1]->getHierarchyDepth(), 2);

    //     // Test Prev sibling
    //     $this->assertEquals($this->nodes[2]->getHierarchyParentId(), 2);
    //     $this->assertEquals($this->nodes[2]->getHierarchyLeftId(), 5);
    //     $this->assertEquals($this->nodes[2]->getHierarchyRightId(), 6);
    //     $this->assertEquals($this->nodes[2]->getHierarchyDepth(), 3);

    //     // Test child
    //     $this->assertEquals($this->nodes[3]->getHierarchyParentId(), 2);
    //     $this->assertEquals($this->nodes[3]->getHierarchyLeftId(), 3);
    //     $this->assertEquals($this->nodes[3]->getHierarchyRightId(), 4);
    //     $this->assertEquals($this->nodes[3]->getHierarchyDepth(), 3);
    // }

    // /**
    //  * Tests previously discovered issue with moving an item under another item
    //  * that makes it's new position a sibling of it's previous parent.
    //  *
    //  * @return void
    //  */
    // public function test_put_after_no_move()
    // {
    //     $this->generateSimpleNodeTree();

    //     $this->assertEquals($this->nodes[0]->descendantNodes()->count(), 3);

    //     $this->nodes[3]->putAfter($this->nodes[2]);

    //     $this->refreshTree();

    //     // Root Element should stay the same;
    //     $this->assertNull($this->nodes[0]->getHierarchyParentId());
    //     $this->assertEquals($this->nodes[0]->getHierarchyLeftId(), 1);
    //     $this->assertEquals($this->nodes[0]->getHierarchyRightId(), 8);
    //     $this->assertEquals($this->nodes[0]->getHierarchyDepth(), 1);

    //     // Test Prev Parent
    //     $this->assertEquals($this->nodes[1]->getHierarchyParentId(), 1);
    //     $this->assertEquals($this->nodes[1]->getHierarchyLeftId(), 2);
    //     $this->assertEquals($this->nodes[1]->getHierarchyRightId(), 7);
    //     $this->assertEquals($this->nodes[1]->getHierarchyDepth(), 2);

    //     // Test Prev sibling
    //     $this->assertEquals($this->nodes[2]->getHierarchyParentId(), 2);
    //     $this->assertEquals($this->nodes[2]->getHierarchyLeftId(), 3);
    //     $this->assertEquals($this->nodes[2]->getHierarchyRightId(), 4);
    //     $this->assertEquals($this->nodes[2]->getHierarchyDepth(), 3);

    //     // Test child
    //     $this->assertEquals($this->nodes[3]->getHierarchyParentId(), 2);
    //     $this->assertEquals($this->nodes[3]->getHierarchyLeftId(), 5);
    //     $this->assertEquals($this->nodes[3]->getHierarchyRightId(), 6);
    //     $this->assertEquals($this->nodes[3]->getHierarchyDepth(), 3);
    // }

    // /**
    //  * Tests that moving a node after a root node sets the parent Id to null
    //  * properly
    //  *
    //  * @return void
    //  */
    // public function test_put_after_root_node()
    // {
    //     $this->generateComplexNodeTree();

    //     $this->nodes[12]->putAfter($this->nodes[11]);

    //     $this->refreshTree();

    //     $this->assertNull($this->nodes[12]->getHierarchyParentId());
    // }
}
