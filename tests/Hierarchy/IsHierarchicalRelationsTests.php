<?php

use Yadda\Enso\Utilities\Tests\TestCase;

class IsHierarchicalRelationsTests extends TestCase
{
    // /**
    //  * Tests that the root node relations work as expected.
    //  * @return boolean [description]
    //  */
    // public function test_that_a_node_has_a_root_node()
    // {
    //     $this->generateSimpleNodeTree();

    //     // Test on root node.
    //     $this->assertNotNull($this->nodes[0]->rootNode());
    //     $this->assertEquals($this->nodes[0]->rootNode()->getKey(), $this->nodes[0]->getKey());

    //     // Test on child of root node
    //     $this->assertNotNull($this->nodes[1]->rootNode());
    //     $this->assertEquals($this->nodes[1]->rootNode()->getKey(), $this->nodes[0]->getKey());

    //     // Test on non-child descendant of root node
    //     $this->assertNotNull($this->nodes[2]->rootNode());
    //     $this->assertEquals($this->nodes[2]->rootNode()->getKey(), $this->nodes[0]->getKey());

    //     // Test check function
    //     $this->assertTrue($this->nodes[0]->isRootNode());
    //     $this->assertFalse($this->nodes[1]->isRootNode());
    //     $this->assertFalse($this->nodes[2]->isRootNode());
    // }

    // public function test_that_a_node_has_a_parent_node()
    // {
    //     $this->generateSimpleNodeTree();

    //     // Test on node with no parent.
    //     $this->assertNull($this->nodes[0]->parentNode());

    //     // Test on node of depth 2
    //     $this->assertNotNull($this->nodes[1]->parentNode());
    //     $this->assertEquals($this->nodes[1]->parentNode()->getKey(), $this->nodes[0]->getKey());

    //     // Test node of depth 3
    //     $this->assertNotNull($this->nodes[2]->parentNode());
    //     $this->assertEquals($this->nodes[2]->parentNode()->getKey(), $this->nodes[1]->getKey());
    // }

    // public function test_that_a_node_has_siblings()
    // {
    //     $this->generateSimpleNodeTree();

    //     // Test a node with no siblings
    //     $this->assertEquals($this->nodes[0]->siblingNodes()->count(), 0);

    //     // Test a node with siblings
    //     $this->assertEquals($this->nodes[2]->siblingNodes()->count(), 1);

    //     // Test check function
    //     $this->assertFalse($this->nodes[0]->hasSiblingNodes());
    //     $this->assertTrue($this->nodes[2]->hasSiblingNodes());
    //     $this->assertFalse($this->nodes[2]->hasSiblingNodes(2));
    //     $this->assertTrue($this->nodes[2]->hasSiblingNodes(1));
    // }

    // public function test_that_a_node_has_children()
    // {
    //     $this->generateSimpleNodeTree();

    //     // Test a node with one child
    //     $this->assertEquals($this->nodes[0]->childNodes()->count(), 1);

    //     // Test a node with many children
    //     $this->assertEquals($this->nodes[1]->childNodes()->count(), 2);

    //     // Test a node with no children
    //     $this->assertEquals($this->nodes[2]->childNodes()->count(), 0);

    //     // Test check function
    //     $this->assertFalse($this->nodes[2]->hasChildNodes());
    //     $this->assertTrue($this->nodes[1]->hasChildNodes());
    //     $this->assertFalse($this->nodes[1]->hasChildNodes(1));
    //     $this->assertTrue($this->nodes[1]->hasChildNodes(2));
    // }

    // public function test_that_a_node_has_descendants()
    // {
    //     $this->generateSimpleNodeTree();

    //     // Test a node with no descendants
    //     $this->assertEquals($this->nodes[2]->descendantNodes()->count(), 0);

    //     // Test a node with only direct descendants
    //     $this->assertEquals($this->nodes[1]->descendantNodes()->count(), 2);

    //     // Test a node with deep descendants
    //     $this->assertEquals($this->nodes[0]->descendantNodes(0)->count(), 3);

    //     // Test check function
    //     $this->assertFalse($this->nodes[2]->hasDescendantNodes());
    //     $this->assertTrue($this->nodes[1]->hasDescendantNodes());
    //     $this->assertFalse($this->nodes[1]->hasDescendantNodes(2));
    //     $this->assertTrue($this->nodes[0]->hasDescendantNodes(2));
    // }
}
