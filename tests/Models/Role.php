<?php

namespace Yadda\Enso\Utilities\Tests\Models;

use Illuminate\Database\Eloquent\Model;

use Yadda\Enso\Utilities\Hierarchy\Contracts\IsHierarchical as IsHierarchicalContract;
use Yadda\Enso\Utilities\Hierarchy\Traits\IsHierarchical;

/**
 * Dummy model for use both as an example and in Tests
 */
class Role extends Model implements IsHierarchicalContract {

    use IsHierarchical;

    protected $fillable = [
        'name',
        'slug',
        'description'
    ];
}
