<?php

namespace Yadda\Enso\Utilities\Tests\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Yadda\Enso\Utilities\Models\Role as Model;

class RoleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Model::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'slug' => $this->faker->unique()->slug,
            'description' => $this->faker->sentence,
            'parent_id' => null,
            'left_id' => 0,
            'right_id' => 0,
            'depth' => 0
        ];
    }
}
