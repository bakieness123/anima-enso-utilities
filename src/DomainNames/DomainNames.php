<?php 

namespace Yadda\Enso\Utilities\DomainNames;

use Illuminate\Support\Facades\Facade;
use Yadda\Enso\Utilities\DomainNames\DomainNamesWorker;

class DomainNames extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return DomainNamesWorker::class;
    }
}