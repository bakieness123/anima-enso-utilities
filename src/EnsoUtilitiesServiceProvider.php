<?php

namespace Yadda\Enso\Utilities;

use Illuminate\Support\ServiceProvider;
use View;
use Yadda\Enso\Utilities\Alerts\Alerts;
use Yadda\Enso\Utilities\Hierarchy\Contracts\HierarchicalRepository as HierarchicalRepositoryContract;

class EnsoUtilitiesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/Hierarchy/config/hierarchies.php',
            'enso-utilities.hierarchies'
        );

        $this->loadViewsFrom(__DIR__ . '/../resources/views/alerts', 'enso-alerts');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(HierarchicalRepositoryContract::class, function ($app, $arguments = []) {
            $role_repository_concrete_class = config('enso-utilities.hierarchies.default_repository_class');
            return new $role_repository_concrete_class(...$arguments);
        });

        $this->app->singleton('enso-alerts', function () {
            return new Alerts;
        });
    }
}
