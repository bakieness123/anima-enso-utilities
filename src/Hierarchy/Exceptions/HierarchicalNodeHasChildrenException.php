<?php

namespace Yadda\Enso\Utilities\Hierarchy\Exceptions;

use Exception;

class HierarchicalNodeHasChildrenException extends Exception {

    //

}
