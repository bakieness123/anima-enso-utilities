<?php

namespace Yadda\Enso\Utilities\Hierarchy\Repositories;

use Yadda\Enso\Utilities\Hierarchy\Contracts\HierarchicalRepository as HierarchicalRepositoryContract;
use Yadda\Enso\Utilities\Hierarchy\Helpers\HierarchyQueryBuilder;
use Yadda\Enso\Utilities\Hierarchy\Contracts\IsHierarchical;
use Yadda\Enso\Utilities\Hierarchy\Helpers\HierarchyTree;

use Illuminate\Support\Collection;

use Exception;

/**
 * Repository for creating and deleting IsHierarchical items, and moving them
 * around their tree structure.
 */
class HierarchyRepository implements HierarchicalRepositoryContract {

    protected $current_node;

    protected $node_class;

    protected $parent_id_column_name;
    protected $left_id_column_name;
    protected $right_id_column_name;
    protected $depth_column_name;

    // Relations to the current node;
    protected $root;
    protected $parent;
    protected $siblings;
    protected $children;
    protected $ascendants;
    protected $descendants;

    // Parent can be null for root elements, so there needs to be a way to know
    // it's been checked
    protected $parent_checked = false;

    public function __construct($current_node)
    {
        if (is_object($current_node)) {
            // Any other functionality?
        } else {
            $current_node = new $current_node;
        }

        $this->parent_id_column_name = $current_node->getParentIdColumnName();
        $this->left_id_column_name = $current_node->getLeftIdColumnName();
        $this->right_id_column_name = $current_node->getRightIdColumnName();
        $this->depth_column_name = $current_node->getDepthColumnName();

        $this->current_node = $current_node;
        $this->node_class = get_class($this->current_node);
    }

    /**
     * Gets a fresh query builder
     *
     * @return HierarchyQueryBuilder
     */
    protected function getQueryBuilder()
    {
        return new HierarchyQueryBuilder($this->current_node);
    }

    /**
     * Parent Id of the current Node
     *
     * @return integer              parent id of current node. Null means root
     */
    protected function parentId()
    {
        return $this->current_node->getHierarchyParentId();
    }

    /**
     * Left id of current Node
     *
     * @return integer              left id of current node
     */
    protected function leftId()
    {
        return $this->current_node->getHierarchyLeftId();
    }

    /**
     * Right id of current Node
     *
     * @return integer              right id of current node
     */
    protected function rightId()
    {
        return $this->current_node->getHierarchyRightId();
    }

    /**
     * Depth of current node
     *
     * @return integer              depth of current node
     */
    protected function depth()
    {
        return $this->current_node->getHierarchyDepth();
    }

    /**
     * Gets a list of all Nodes that are higher up (but still related) in the
     * current tree node.
     *
     * @return Collection                   Ascendant nodes
     */
    public function ascendants()
    {
        if (is_null($this->ascendants)) {
            $this->ascendants = $this->node_class::hierarchyAscendantsOf($this->current_node)
                ->orderBy($this->left_id_column_name, 'ASC')
                ->get();
        }

        return $this->ascendants;
    }

    /**
     * Returns the current_node if it is a root node. Otherwise, returns the
     * first element of the ascendantNodes collection.
     *
     * @return IsHierarchical               Root Node, or self
     */
    public function root()
    {
        if ($this->current_node->isRootNode()) {
            return $this->current_node;
        }

        if (is_null($this->root)) {
            if($this->ascendants()->count() === 0) {
                throw new Exception("Broken Hierarchy Node with parent_id but no ascendants. ID: {$this->current_node->getKey()}");
            }

            $this->root = $this->ascendants()->first();
        }

        return $this->root;
    }

    /**
     * Grabs the last element on the ascendantNodes collection to return as the
     * parent of the current node. If his is a root node, just returns null
     *
     * @return IsHierarchical|null          Parent Node, of null if root.
     */
    public function parent()
    {
        if ($this->current_node->isRootNode()) {
            return null;
        }

        if (is_null($this->parent) && !$this->parent_checked) {
            if($this->ascendants()->count() === 0) {
                throw new Exception("Broken Hierarchy Node with parent_id but no ascendants. ID: {$this->current_node->getKey()}");
            }

            $this->parent = $this->ascendants()->last();
            $this->parent_checked = true;
        }

        return $this->parent;
    }

    /**
     * Returns a collection of all of the siblings of the current_node
     *
     * @return Collection                   Siblings
     */
    public function siblings()
    {
        if (is_null($this->siblings)) {
            $this->siblings = $this->node_class::hierarchySiblingsOf($this->current_node)->get();
        }

        return $this->siblings;
    }

    /**
     * Returns a collection of all of the descendants of the current_node
     *
     * @return Collection                   Descendants
     */
    public function descendants()
    {

        if (is_null($this->descendants)) {
            $this->descendants = $this->node_class::hierarchyDescendantsOf($this->current_node)
                ->orderBy($this->depth_column_name, 'ASC')
                ->orderBy($this->left_id_column_name, 'ASC')
                ->get();
        }

        return $this->descendants;
    }

    /**
     * Filters the descendants list (to avoid requerying the same data) to
     * provide a list of direct children of the current_node
     *
     * @return Collection                   Children
     */
    public function children()
    {
        if (is_null($this->children)) {
            $this->children = $this->descendants()->filter(function($descendant) {
                return ((int) $descendant->getHierarchyDepth() === (int) $this->current_node->getHierarchyDepth() + 1);
            });
        }

        return $this->children;
    }

    /**
     * Gets the current highest right_id in the tree for a given class
     *
     * @param  string       $class      IsHierarchical node class
     * @return integer                  current max right id
     */
    protected static function getHierarchyMaxRightId($class)
    {
        $temporary_instance = new $class;
        $temporary_instance = $class::orderBy($temporary_instance->getRightIdColumnName(), 'DESC')->first();
        return is_null($temporary_instance) ? 0 : $temporary_instance->getHierarchyRightId();
    }

    /**
     * Queries the DB to get all the nodes required to build the tree structure,
     * then build it with php. This will be far faster than any other kind of
     * recursive query that build it all in one go, especially if the tree
     * structure should get deep.
     *
     * @param  [type] $class [description]
     * @return [type]        [description]
     */
    public static function buildTree($node)
    {
        // @todo - make accept more things

        if(!is_object($node)) {
            $node = new $node;
        }

        $node_class = get_class($node);

        $nodes = $node_class::orderBy($node->getLeftIdColumnName(), 'ASC')->get();

        $tree = new HierarchyTree($nodes);

        return $tree;
    }

    protected function convertNodeCollection($nodes)
    {
        return $nodes;
    }

    /**
     * Creates a new node of the class passed in. This will initially set it up
     * as a new root node.
     *
     * @param  string           $class          Class of node to create
     * @param  array            $attributes     Attributes to set
     * @return IsHierarchical                   created node
     */
    public static function create($class, $attributes = [])
    {
        $new_node = new $class;

        foreach ($attributes as $attribute => $value) {
            if ($new_node->isFillable($attribute)) $new_node->$attribute = $value;
        }

        $max_right_id = static::getHierarchyMaxRightId($new_node);

        if(!isset($attribute[$new_node->getParentIdColumnName()])) $new_node->setHierarchyParentId(null);
        if(!isset($attribute[$new_node->getLeftIdColumnName()])) $new_node->setHierarchyLeftId($max_right_id + 1);
        if(!isset($attribute[$new_node->getRightIdColumnName()])) $new_node->setHierarchyRightId($max_right_id + 2);
        if(!isset($attribute[$new_node->getDepthColumnName()])) $new_node->setHierarchyDepth(1);

        $new_node->save();

        return $new_node;
    }

    /**
     * Deletes a given node, and if specified, it's children.
     *
     * @param  IsHierarchical   $node               Node to delete
     * @param  boolean          $delete_children    Whether to delete branch or
     *                                              node
     * @return boolean                              Success status
     */
    public static function delete(IsHierarchical $node, $delete_children = false)
    {
        if($delete_children) {
            return $node->deleteBranch();
        }

        return $node->deleteNode();
    }

    /**
     * Defers call to dedicated Hierarchical Query helper
     *
     * @param  IsHierarchical   $node           Node that will house the moved node
     * @return boolean                          Success status
     */
    public function moveNodeUnderNode(IsHierarchical $node)
    {
        return $this->getQueryBuilder()->moveNodeUnderNode($node);
    }

    /**
     * Defers call to dedicated Hierarchical Query helper
     *
     * @param  IsHierarchical   $node           Node that will house the moved node
     * @return boolean                          Success status
     */
    public function moveNodeAfterNode(IsHierarchical $node)
    {
        return $this->getQueryBuilder()->moveNodeAfterNode($node);
    }

    /**
     * Defers call to dedicated Hierarchical Query helper
     *
     * @param  IsHierarchical   $node           Node that will house the moved node
     * @return boolean                          Success status
     */
    public function moveNodeBeforeNode(IsHierarchical $node)
    {
        return $this->getQueryBuilder()->moveNodeBeforeNode($node);
    }

    /**
     * Defers call to dedicated Hierarchical Query helper
     *
     * @return boolean                          Success status
     */
    public function convertToRootNode()
    {
        return $this->getQueryBuilder()->convertNodeToRootNode();
    }

    /**
     * Deletes the current node. Throws an exception if the current node has any
     * descendants
     *
     * @return boolean                          Success status
     */
    public function deleteNode()
    {
        return $this->getQueryBuilder()->removeNodeFromTree();
    }

    /**
     * Deletes the current node and all it's children
     *
     * @return Boolean                          Success status
     */
    public function deleteBranch()
    {
        return $this->getQueryBuilder()->removeBranchFromTree();
    }
}
