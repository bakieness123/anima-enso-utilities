<?php

namespace Yadda\Enso\Utilities\Hierarchy\Contracts;

interface IsHierarchical {

    /**
     * Gets name of parent_id column. Allows for overriding if other column name
     * desired.
     *
     * @return string                   parent_id column name
     */
    function getParentIdColumnName();

    /**
     * Gets the parent id of the current node
     *
     * @return integer                  parent_id or null (if root node)
     */
    function getHierarchyParentId();

    /**
     * Gets name of left_id column. Allows for overriding if other column name
     * desired.
     *
     * @return string                   left_id column name
     */
    function getLeftIdColumnName();

    /**
     * Gets the left id of the current node
     *
     * @return integer                  left_id
     */
    function getHierarchyLeftId();

    /**
     * Gets name of right_id column. Allows for overriding if other column name
     * desired.
     *
     * @return string                   right_id column name
     */
    function getRightIdColumnName();

    /**
     * Gets the right id of the current node
     *
     * @return integer                  right_id
     */
    function getHierarchyRightId();

    /**
     * Gets name of depth column. Allows for overriding if other column name
     * desired.
     *
     * @return string                   depth column name
     */
    function getDepthColumnName();

    /**
     * Gets the depth id of the current node
     *
     * @return integer                  depth
     */
    function getHierarchyDepth();

    /**
     * Gets an instance of the HierarchyRepository for moving this item
     * within it's hierarchy tree.
     *
     * @param  bolearn              $reload     Force Hierarchy reload.
     * @return HierarchyRepository
     */
    function getHierarchyRepository($reload = false);

    /**
     * Boolean check to see whether this node is a root node
     *
     * @return boolean
     */
    function isRootNode();

    /**
     * Gets the root node of this node, or self if it IS the root node
     *
     * @return IsHierarchical           Root Node
     */
    function rootNode();

    /**
     * Gets the parent node of this node.
     *
     * @return IsHierarchical           Parent node
     */
    function parentNode();

    /**
     * Boolean check to see whether this node has any siblings
     *
     * @param  integer      $number     Specific number of siblings to check for
     * @return boolean
     */
    function hasSiblingNodes($number = null);

    /**
     * Gets the siblings nodes of this node
     *
     * @return Collection           Sibling nodes
     */
    function siblingNodes();

    /**
     * Boolean check to see whether this node has any direct children
     *
     * @param  integer      $number     Specific number of siblings to check for
     * @return boolean
     */
    function hasChildNodes($number = null);

    /**
     * Gets the direct children of this node
     *
     * @return Collection           Children nodes
     */
    function childNodes();

    /**
     * Boolean check to see whether this node has any descendants. Optionally,
     * You can pass in a depth to limit the check to only descendants as least
     * $depth levels deep. (direct children = 1, grand children = 2 etc...)
     *
     * @param  integer      $min_depth      minimum depth to be counted
     * @return boolean
     */
    function hasDescendantNodes($min_depth = 1);

    /**
     * Gets all the ascendant nodes of this node
     *
     * @return Collection           Descendant nodes
     */
    function ascendantNodes();

    /**
     * Gets all the descendant nodes of this node
     *
     * @return Collection           Descendant nodes
     */
    function descendantNodes();

    /**
     * Query filter to return only the root node for the given node
     *
     * @param  Builder          $query  Original query
     * @param  IsHierarchical   $node   Node to find root of
     * @return Builder                  Modified query
     */
    function scopeHierarchyRootOf($query, IsHierarchical $node);

    /**
     * Query filter to return only the parent node for the given node
     *
     * @param  Builder          $query  Original query
     * @param  IsHierarchical   $node   Node to find parent of
     * @return Builder                  Modified query
     */
    function scopeHierarchyParentOf($query, IsHierarchical $node);

    /**
     * Query filter to return only the sibling nodes for the given node
     *
     * @param  Builder          $query  Original query
     * @param  IsHierarchical   $node   Node to find siblings of
     * @return Builder                  Modified query
     */
    function scopeHierarchySiblingsOf($query, IsHierarchical $node);

    /**
     * Query filter to return only the child nodes for the given node
     *
     * @param  Builder          $query  Original query
     * @param  IsHierarchical   $node   Node to find childrens of
     * @return Builder                  Modified query
     */
    function scopeHierarchyChildrenOf($query, IsHierarchical $node);

    /**
     * Query filter to return only the descendant nodes for the given node
     *
     * @param  Builder          $query  Original query
     * @param  IsHierarchical   $node   Node to find ascendants of
     * @return Builder                  Modified query
     */
    function scopeHierarchyAscendantsOf($query, IsHierarchical $node);

    /**
     * Query filter to return only the descendant nodes for the given node
     *
     * @param  Builder          $query  Original query
     * @param  IsHierarchical   $node   Node to find descendants of
     * @return Builder                  Modified query
     */
    function scopeHierarchyDescendantsOf($query, IsHierarchical $node);
}
