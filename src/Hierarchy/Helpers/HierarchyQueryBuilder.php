<?php

namespace Yadda\Enso\Utilities\Hierarchy\Helpers;

use Carbon\Carbon;

use Exception;
use Log;
use DB;

use Yadda\Enso\Utilities\Hierarchy\Exceptions\HierachicalQueryNoMoveRequiredException;
use Yadda\Enso\Utilities\Hierarchy\Exceptions\HierarchicalNodeHasChildrenException;
use Yadda\Enso\Utilities\Hierarchy\Exceptions\HierarchicalNodeInvalidMoveType;

use Yadda\Enso\Utilities\Hierarchy\Helpers\HierarchicalQueryUpdateStatement;
use Yadda\Enso\Utilities\Hierarchy\Helpers\HierarchicalQueryUpdateColumn;

use Yadda\Enso\Utilities\Hierarchy\Helpers\QueryGenerators\MoveBeforeQueryGenerator;
use Yadda\Enso\Utilities\Hierarchy\Helpers\QueryGenerators\MoveUnderQueryGenerator;
use Yadda\Enso\Utilities\Hierarchy\Helpers\QueryGenerators\MoveAfterQueryGenerator;

use Yadda\Enso\Utilities\Hierarchy\Contracts\IsHierarchical;

/**
 * A Helper class that can build and complete complex queries based around the
 * heirarchical tree structure.
 */
class HierarchyQueryBuilder {

    protected $source_node;
    protected $destination_node;
    protected $node_class;
    protected $table;

    protected $parent_column_name;
    protected $left_column_name;
    protected $right_column_name;
    protected $depth_column_name;

    protected $cases = [];
    protected $modifiers = [];
    protected $new_values = [];
    protected $where;

    protected $update_statement;

    /**
     * Sets some internal values to save constantly checking them against the
     * node given.
     *
     * @param IsHierarchical    $node           node to use as source node.
     */
    public function __construct(IsHierarchical $node)
    {
        $this->source_node = $node;
        $this->node_class = get_class($node);

        $this->table = $node->getTable();
        $this->old_table = $this->table === 'old' ? 'older' : 'old';

        $this->parent_column_name = $node->getParentIdColumnName();
        $this->left_column_name = $node->getLeftIdColumnName();
        $this->right_column_name = $node->getRightIdColumnName();
        $this->depth_column_name = $node->getDepthColumnName();

        $this->update_statement = new HierarchicalQueryUpdateStatement($this->table, $this->old_table, $this->source_node->getKeyName());
    }

    /**
     * Sets the destination_node property after checking that the given node is
     * of the correct class
     *
     * @param IsHierarchical    $node           node to use as destination node
     */
    protected function setDestinationNode(IsHierarchical $node)
    {
        if (!$node instanceof $this->node_class) {
            throw new Exception('Failed moving taxonomy node: Mismatched types given');
        }

        if ($this->nodeIsDescendantOfNode($node, $this->source_node)) {
            throw new Exception('Failed moving taxonomy node: Cannot move a node underneath itself');
        }

        $this->destination_node = $node;
    }

    /**
     * Test to see whether a given node is a current descendant of another given
     * node, without referencing the database
     *
     * @param  IsHierarchical   $possible_descendant    Potential descendant
     * @param  IsHierarchical   $node                   Node to test against
     * @return boolean                                  Whether is descendant
     */
    protected function nodeIsDescendantOfNode($possible_descendant, $node)
    {
        return (
            $node->getHierarchyLeftId() < $possible_descendant->getHierarchyLeftId() &&
            $node->getHierarchyRightId() > $possible_descendant->getHierarchyRightId()
        );
    }

    /**
     * Gets the minimum value from the source and destination left ids
     *
     * @param  integer      $modifier           ammount to modify value by
     * @return integer
     */
    protected function minLeft($modifier = 0)
    {
        return min(
            $this->source_node->getHierarchyLeftId(),
            $this->destination_node->getHierarchyLeftId()
        ) + $modifier;
    }

    /**
     * Gets the maximum value from the source and destination right ids
     *
     * @param  integer      $modifier           ammount to modify value by
     * @return integer
     */
    protected function maxRight($modifier = 0)
    {
        return max(
            $this->source_node->getHierarchyRightId(),
            $this->destination_node->getHierarchyRightId()
        ) + $modifier;
    }

    /**
     * Moves a node from it's current position to the being the new 'last child'
     * of a given node, reordering the tree as required.
     *
     * @param  IsHierarchical   $node           node to insert current node under
     * @return boolean                          success status
     */
    public function moveNodeUnderNode(IsHierarchical $node)
    {
        try {
            $generator = new MoveUnderQueryGenerator($this->source_node, $node);

            return DB::statement(DB::raw($generator->generateStatement()));
        } catch (HierachicalQueryNoMoveRequiredException $e) {
            return true;
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    /**
     * Moves a node from it's current position to the being the next node in the
     * tree after a given node, reordering the tree as required.
     *
     * @param  IsHierarchical   $node           node to insert current node after
     * @return boolean                          success status
     */
    public function moveNodeAfterNode(IsHierarchical $node)
    {
        try {
            $generator = new MoveAfterQueryGenerator($this->source_node, $node);

            return DB::statement(DB::raw($generator->generateStatement()));
        } catch (HierachicalQueryNoMoveRequiredException $e) {
            return true;
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    /**
     * Moves a node from it's current position to the being the previous node in
     * the tree after a given node, reordering the tree as required.
     *
     * @param  IsHierarchical   $node           node to insert current node before
     * @return boolean                          success status
     */
    public function moveNodeBeforeNode($node)
    {
        try {
            $generator = new MoveBeforeQueryGenerator($this->source_node, $node);

            return DB::statement(DB::raw($generator->generateStatement()));
        } catch (HierachicalQueryNoMoveRequiredException $e) {
            return true;
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }

    /**
    * Moves a node from it's position in the tree and converts in into a root
    * node in the last position in the tree. If moving the node required it to
    * be anything other than the new last root node in the tree, use either
    * putAfter or putBefore to positioni as required.
    *
    * @return boolean                              Success status
    */
    public function convertNodeToRootNode()
    {
        // As a tree is ordered by left and right id, DESC ordered right id will
        // return the tree backwards. compared to left id. This means the first
        // element will be the last rood node.
        $last_root = $this->node_class::orderBy($this->right_column_name, 'DESC')->first();

        try {
            // No need for custom code, just defer to existing call.
            return $this->moveNodeAfterNode($last_root);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Sets common parameters, or common defaults which can be overriden for
     * specific cases.
     */
    protected function setDefaultParameters()
    {
        $this->cases['moved_case'] = $this->getMovedCase($this->source_node);
        $this->cases['moved_node_only_case'] = $this->getMovedNodeCase($this->source_node);

        // Modifier that can be applied to left, right or both ids of elements
        // that were affected by moving other elements around the tree.
        $this->modifiers['changed_ids'] = ($this->source_node->getHierarchyRightId()  - $this->source_node->getHierarchyLeftId()) + 1;

        if (!is_null($this->destination_node)) {
            $this->cases['nodes_between_case'] = $this->getNodesBetweenCase(
                $this->source_node,
                $this->destination_node
            );

            // Value to modify depth of moved items by
            $this->modifiers['depth'] = ($this->destination_node->getHierarchyDepth() - $this->source_node->getHierarchyDepth());

            // null as a value can't be used in MySQL queries, so this converts
            // a null value into a useable "NULL" string.
            $this->new_values['parent_id'] = is_null($this->destination_node->getHierarchyParentId()) ?
                "NULL" :
                (string) $this->destination_node->getHierarchyParentId();

            // Used to rule out unchanged rows from the query, to help keep the
            // query lean
            $this->where = "{$this->old_table}.{$this->left_column_name} BETWEEN {$this->minLeft()} AND {$this->maxRight()} OR {$this->old_table}.{$this->right_column_name} BETWEEN {$this->minLeft()} AND {$this->maxRight()}";
        }
    }

    /**
     * Sets or overrides parameters specific to teh node-before-node move type
     */
    protected function setNodeBeforeNodeParameters()
    {
        // Moving a node left or right have different conditions and update
        // requirements.
        if ($this->moveIsLeft('before')) {
            // Ensure that the item is actually moving.
            $complete_moved_id_modifier = abs($this->destination_node->getHierarchyLeftId() - $this->source_node->getHierarchyLeftId());
            if($complete_moved_id_modifier === 0) {
                throw new HierachicalQueryNoMoveRequiredException();
            }

            $this->modifiers['changed'] = "+ {$this->modifiers['changed_ids']}";
            $this->modifiers['moved'] = "- {$complete_moved_id_modifier}";
            $this->cases['update_left_only_case'] = $this->getAscendantsCase($this->source_node);

            // Include the destination node if the source node is a descendant
            // of it
            $this->cases['update_right_only_case'] = $this->getAscendantsCase(
                $this->destination_node,
                !$this->nodeIsDescendantOfNode($this->source_node, $this->destination_node)
            );

            $this->cases['nodes_between_case'] = $this->getNodesBetweenCase(
                $this->source_node,
                $this->destination_node,
                ['include_low']
            );
        } else {
            // Ensure that the item is actually moving.
            $complete_moved_id_modifier = ($this->destination_node->getHierarchyLeftId() - (1 + $this->source_node->getHierarchyRightId()));
            if($complete_moved_id_modifier === 0) {
                throw new HierachicalQueryNoMoveRequiredException();
            }

            $this->modifiers['changed'] = "- {$this->modifiers['changed_ids']}";
            $this->modifiers['moved'] = "+ {$complete_moved_id_modifier}";

            // Include the destination node if the source node is a descendant
            // of it
            $this->cases['update_left_only_case'] = $this->getAscendantsCase(
                $this->destination_node,
                !$this->nodeIsDescendantOfNode($this->source_node, $this->destination_node)
            );
            $this->cases['update_right_only_case'] = $this->getAscendantsCase($this->source_node);
            $this->cases['nodes_between_case'] = $this->getNodesBetweenCase(
                $this->source_node,
                $this->destination_node
            );
        }
    }

    /**
     * Determines whether this move is left or right through the tree for a
     * given move type.
     *
     * @param  string       $move_type          move type
     * @return boolean                          true for left, false for right
     */
    protected function moveIsLeft($move_type)
    {
        switch ($move_type) {
            case 'under':
                return ((int) $this->destination_node->getHierarchyRightId() < (int) $this->source_node->getHierarchyRightId());
            case 'after':
                return ((int) $this->destination_node->getHierarchyRightId() < (int) $this->source_node->getHierarchyRightId());
            case 'before':
                return ((int) $this->destination_node->getHierarchyLeftId() < (int) $this->source_node->getHierarchyLeftId());
            default:
                throw new Exception("Unknown move type '{$move_type}'");
        }
    }

    /**
     * Generates a MySQL query string describing the moving on a node (and it's
     * children, if present) to a position relation to another node, base on
     * the values set by previous function calls.
     *
     * @return string                           MySQL query
     */
    protected function generateMoveStatement()
    {
        // Need to manually set the updated_at, as this isn't done through
        // Eloquent
        $now = Carbon::now();

        $this->update_statement
            ->addColumn((new HierarchicalQueryUpdateColumn($this->left_column_name))
                ->addModifyClause($this->cases['moved_case'], $this->modifiers['moved'])
                ->addModifyClause($this->cases['nodes_between_case'], $this->modifiers['changed'])
                ->addModifyClause($this->cases['update_left_only_case'], $this->modifiers['changed']))
            ->addColumn((new HierarchicalQueryUpdateColumn($this->right_column_name))
                ->addModifyClause($this->cases['moved_case'], $this->modifiers['moved'])
                ->addModifyClause($this->cases['nodes_between_case'], $this->modifiers['changed'])
                ->addModifyClause($this->cases['update_right_only_case'], $this->modifiers['changed']))
            ->addColumn((new HierarchicalQueryUpdateColumn($this->parent_column_name))
                ->addSetClause($this->cases['moved_node_only_case'], $this->new_values['parent_id']))
            ->addColumn((new HierarchicalQueryUpdateColumn($this->depth_column_name))
                ->addModifyClause($this->cases['moved_case'], "+ {$this->modifiers['depth']}"))
            ->addColumn((new HierarchicalQueryUpdateColumn("updated_at"))
                ->setValue("'{$now->__toString()}'"))
            ->where($this->where);

        return $this->update_statement->getSql();
    }

    /**
     * Deletes a node from the tree, then updates the left and right ids on
     * the remaining nodes in the tree to the right of the removal location
     *
     * @return boolean                          Success status
     */
    public function removeNodeFromTree() {
        $now = Carbon::now();

        // Cheap way to determine how much space the moved node/branch took up
        // without having to query the database to get number of children
        $changed_nodes_id_modifier = ($this->source_node->getHierarchyRightId() - $this->source_node->getHierarchyLeftId()) + 1;

        // Prevent deleting of a node with children. Had discussion about making
        // the children be children of the nodes parent, but it was decided that
        // we make the user move child nodes first, then delete current node
        // with this call
        if ($changed_nodes_id_modifier > 2) {
            throw new HierarchicalNodeHasChildrenException('Cannot delete a node with children');
        }

        $right_of_removed_case = $this->getNodesRightOfNodeCase($this->source_node);

        // Delete the node through the normal, Eloquent method. This way, the
        // option is still there to customise the delete method as desired.
        $this->source_node->delete();

        // Update the tree where needed.
        $this->update_statement
            ->addColumn((new HierarchicalQueryUpdateColumn($this->left_column_name))
                ->addModifyClause($right_of_removed_case, "- {$changed_nodes_id_modifier}"))
            ->addColumn((new HierarchicalQueryUpdateColumn($this->right_column_name))
                ->setValue("({$this->old_table}.{$this->right_column_name} - {$changed_nodes_id_modifier})"))
            ->addColumn((new HierarchicalQueryUpdateColumn("updated_at"))
                ->setValue("'{$now->__toString()}'"))
            ->where("{$this->old_table}.{$this->right_column_name} > {$this->source_node->getHierarchyRightId()}");

        return DB::statement($this->update_statement->getSql());
    }

    /**
     * Deletes a branch from the tree, then updates the left and right ids on
     * the remaining nodes in the tree to the right of the removal location
     *
     * @return boolean                          Success status
     */
    public function removeBranchFromTree() {
        $now = Carbon::now();

        // Cheap way to determine how much space the moved node/branch took up
        // without having to query the database to get number of children
        $changed_nodes_id_modifier = ($this->source_node->getHierarchyRightId() - $this->source_node->getHierarchyLeftId()) + 1;

        $right_of_removed_case = $this->getNodesRightOfNodeCase($this->source_node);

        // Delete full branch through the normal, Eloquent method. This way, the
        // option is still there to customise the delete method as desired.
        $node_class = get_class($this->source_node);
        $nodes_to_delete = $node_class::hierarchyDescendantsOf($this->source_node)->get();
        foreach ($nodes_to_delete as $node) {
            $node->delete();
        }
        $this->source_node->delete();

        $this->update_statement
            ->addColumn((new HierarchicalQueryUpdateColumn($this->left_column_name))
                ->addModifyClause($right_of_removed_case, "- {$changed_nodes_id_modifier}"))
            ->addColumn((new HierarchicalQueryUpdateColumn($this->right_column_name))
                ->setValue("({$this->old_table}.{$this->right_column_name} - {$changed_nodes_id_modifier})"))
            ->addColumn((new HierarchicalQueryUpdateColumn("updated_at"))
                ->setValue("'{$now->__toString()}'"))
            ->where("{$this->old_table}.{$this->right_column_name} > {$this->source_node->getHierarchyRightId()}");

        return DB::statement($this->update_statement->getSql());
    }

    /**
     * Prepares a MySQL CASE clause for all rows that represent parent nodes of
     * a given node, optionally including the node itself.
     *
     * @param  IsHierarchical   $node           $node to get ascendants of
     * @param  boolean          $include_node   whether to include given node
     * @return string                           case that describes these nodes
     */
    protected function getAscendantsCase(IsHierarchical $node, $include_node = false)
    {
        if ($include_node) {
            return "({$this->old_table}.{$node->getLeftIdColumnName()} <= {$node->getHierarchyLeftId()} AND {$this->old_table}.{$node->getRightIdColumnName()} >= {$node->getHierarchyRightId()})";
        }

        return "({$this->old_table}.{$node->getLeftIdColumnName()} < {$node->getHierarchyLeftId()} AND {$this->old_table}.{$node->getRightIdColumnName()} > {$node->getHierarchyRightId()})";
    }

    /**
     * Prepares a MySQL CASE clause for the row that represents the given node only
     *
     * @param  IsHierarchical   $node   Node to make case for
     * @return string                   Case for given node
     */
    protected function getMovedNodeCase(IsHierarchical $node)
    {
        return "{$this->old_table}.{$node->getKeyName()} = {$node->getKey()}";
    }

    /**
     * Prepares a MySQL CASE clause for all rows that are being moved
     *
     * @param  IsHierarchical   $moving_node    node being moved
     * @return string                           case that describes these nodes
     */
    protected function getMovedCase(IsHierarchical $moving_node)
    {
        return "({$this->old_table}.{$moving_node->getLeftIdColumnName()} >= {$moving_node->getHierarchyLeftId()} AND {$this->old_table}.{$moving_node->getRightIdColumnName()} <= {$moving_node->getHierarchyRightId()})";
    }

    /**
     * Prepares a MySQL CASE clause for all rows that are wholly between the
     * source and destination nodes.
     *
     * @param  IsHierarchical   $source_node        node being moved
     * @param  IsHierarchical   $destination_node   new parent of node being moved
     * @return string                               case that describes the nodes
     *                                              wholly between the two
     */
    protected function getNodesBetweenCase(IsHierarchical $source_node, IsHierarchical $destination_node, $options = [])
    {
        if ($source_node->getHierarchyLeftId() > $destination_node->getHierarchyLeftId()) {
            $low_node = $destination_node;
            $high_node = $source_node;
        } else {
            $low_node = $source_node;
            $high_node = $destination_node;
        }

        $high_operator = in_array('include_high', $options) ? '<=' : '<';
        $low_operator = in_array('include_low', $options) ? '>=' : '>';

        $high_value = $high_node->getHierarchyRightId();
        $low_value = in_array('exclude_low', $options) ? $high_node->getHierarchyLeftId() : $low_node->getHierarchyLeftId();

        return "({$this->old_table}.{$low_node->getLeftIdColumnName()} {$low_operator} {$low_value} AND {$this->old_table}.{$low_node->getRightIdColumnName()} {$high_operator} {$high_value})";
    }

    /**
     * Prepares a MySQL CASE clause for all rows that are fully to the right of
     * the given node
     *
     * @param  IsHierarchical   $node           node being removed
     * @return string                           case that describes the nodes
     *                                          that fully come after the node
     *                                          being deleted.
     */
    protected function getNodesRightOfNodeCase(IsHierarchical $node)
    {
        return "({$this->old_table}.{$node->getLeftIdColumnName()} > {$node->getHierarchyLeftId()} AND {$this->old_table}.{$node->getRightIdColumnName()} > {$node->getHierarchyRightId()})";
    }
}
