<?php

namespace Yadda\Enso\Utilities\Hierarchy\Helpers\QueryGenerators;

use Yadda\Enso\Utilities\Hierarchy\Helpers\HierarchicalQueryUpdateStatement;
use Yadda\Enso\Utilities\Hierarchy\Contracts\IsHierarchical;

abstract class BaseQueryGenerator {

    protected $source_node;

    protected $node_class;
    protected $table_name;
    protected $old_table_name;

    protected $parent_column_name;
    protected $left_column_name;
    protected $right_column_name;
    protected $depth_column_name;

    protected $cases = [];
    protected $modifiers = [];
    protected $new_values = [];
    protected $where_clause;

    protected $statement;

    public function __construct(IsHierarchical $source_node) {
        $this->setSourceNode($source_node);

        $this->statement = new HierarchicalQueryUpdateStatement(
            $this->table_name,
            $this->old_table_name,
            $this->source_node->getKeyName()
        );
    }

    /**
     * Sets all of the data properties associated with the node type passed ins
     * as the source node.
     *
     * @param IsHierarchical    $source_node        Node to use as source
     */
    protected function setSourceNode(IsHierarchical $source_node) {
        $this->source_node = $source_node;
        $this->node_class = get_class($this->source_node);

        $this->parent_column_name = $this->source_node->getParentIdColumnName();
        $this->left_column_name = $this->source_node->getLeftIdColumnName();
        $this->right_column_name = $this->source_node->getRightIdColumnName();
        $this->depth_column_name = $this->source_node->getDepthColumnName();

        // Sets the table to work on
        $this->table_name = $this->source_node->getTable();

        // On the off-chance that the source table is called 'old', set the
        // old_table name to something else.
        //
        // Having an old_table name is a requirement due to MySQL updating
        // columns in series, and so once updated the left_id cannot be relieds
        // upon as a source of truth for determining which right_ids need
        // updating. The workaround is to inner join the table on itself so you
        // can update values while still having access to the old values.
        $this->old_table_name = ($this->table_name === 'old') ? 'older' : 'old';
    }

    /**
     * Generates a MySQL query string describing the moving on a node (and it's
     * children, if present) to a position relation to another node, base on
     * the values set by previous function calls.
     *
     * @return string                           MySQL query
     */
    abstract public function generateStatement();

    /**
     * Prepares a MySQL CASE clause for all rows that represent parent nodes of
     * a given node OR is the node itself.
     *
     * Note: This is used but is 'technically' incorrect for calls made with the
     * intent of finding a list of source_node parents to update. HOWEVER, as
     * the 'source_node' CASE comes before the 'update_left_case' or the
     * 'update_right_case', it is simpler to just use this instaed of a complex
     * conditional query.
     *
     * @param  IsHierarchical   $node           $node to get ascendants of
     * @param  array            $options        Extra query qualifiers
     * @return string                           case that describes these nodes
     */
    protected function getAscendantsCase(IsHierarchical $node, $options = [])
    {
        $right_operator = in_array('exclude_parent_right', $options) ? ">" : ">=";

        $right_value = isset($options['right_value']) ? $options['right_value'] : $node->getHierarchyRightId();

        return
            "(".
                "{$this->old_table_name}.{$this->left_column_name} <= {$node->getHierarchyLeftId()}".
                " AND ".
                "{$this->old_table_name}.{$this->right_column_name} {$right_operator} {$right_value}".
            ")";
    }

    /**
     * Prepares a MySQL CASE clause for the row that represents the source node
     * only.
     *
     * @return string                   Case for given node
     */
    protected function getSourcedNodeCase()
    {
        return "({$this->old_table_name}.{$this->source_node->getKeyName()} = {$this->source_node->getKey()})";
    }

    /**
     * Prepares a MySQL CASE clause for all rows that are being targeted for the
     * move query.
     *
     * @return string                           case that describes these nodes
     */
    protected function getSourceNodeAndDescendantsCase()
    {
        return "({$this->old_table_name}.{$this->left_column_name} >= {$this->source_node->getHierarchyLeftId()} AND {$this->old_table_name}.{$this->right_column_name} <= {$this->source_node->getHierarchyRightId()})";
    }
}
