<?php

namespace Yadda\Enso\Utilities\Alerts\Facades;

use Illuminate\Support\Facades\Facade;

class AlertsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'enso-alerts';
    }
}
