<?php

namespace Yadda\Enso\Utilities\Traits;

use Crypt;
use Exception;
use Illuminate\Support\Str;

/**
 * Trait to modify the default eloquent model to allow for setting of encrypted
 * fields. Most recent Laravel version actively tested for: 5.4.
 */
trait Encryptable
{
    /**
    * Get all decrypted attributes as an array
    * @return array All attributes decypted
    */
    public function attributesToArray()
    {
        $attributes = parent::attributesToArray();

        foreach ($attributes as $key => $value) {
            if (in_array($key, $this->encryptable)) {
                $attributes[$key] = Crypt::decrypt($value);
            }
        }

        return $attributes;
    }

    /**
     * Get an attribute from the model, decrypting
     * if necessary.
     *
     * @param  string  $key Attribute name
     * @return various      The unencrypted attribute
     */
    public function getAttributeValue($key)
    {
        $value = parent::getAttributeValue($key);

        if (in_array($key, $this->encryptable)) {
            $value = Crypt::decrypt($value);
        }

        return $value;
    }

    /**
     * Set an attribute, encrypting if appropriate. Anything with it's own
     * mutator is assumed to take control of the encrypting requirements. We
     * do not intend to perform encryption on JSON data at this time.
     *
     * @param string  $key   Attribute name
     * @param various $value Attribute value
     */
    public function setAttribute($key, $value)
    {
        // First we will check for the presence of a mutator for the set operation
        // which simply lets the developers tweak the attribute as it is set on
        // the model, such as "json_encoding" an listing of data for storage.
        if ($this->hasSetMutator($key)) {
            $method = 'set'.Str::studly($key).'Attribute';

            return $this->{$method}($value);
        }

        // If an attribute is listed as a "date", we'll convert it from a DateTime
        // instance into a form proper for storage on the database tables using
        // the connection grammar's date format. We will auto set the values.
        elseif ($value && $this->isDateAttribute($key)) {
            $value = $this->fromDateTime($value);
        }

        if ($this->isJsonCastable($key) && ! is_null($value)) {
            $value = $this->castAttributeAsJson($key, $value);
        }

        // If this attribute contains a JSON ->, we'll set the proper value in the
        // attribute's underlying array. This takes care of properly nesting an
        // attribute in the array's value in the case of deeply nested items.
        if (Str::contains($key, '->')) {
            if (in_array($key, $this->encryptable)) {
                throw new Exception("No Encryption of Json data available at present");
            }
            return $this->fillJsonAttribute($key, $value);
        }

        if (in_array($key, $this->encryptable)) {
            $value = Crypt::encrypt($value);
        }

        $this->attributes[$key] = $value;

        return $this;
    }

    /**
     * Get all of the current attributes on the model, Decrypting where required
     *
     * @return array
     */
    public function getAttributes()
    {
        $values = parent::getAttributes();

        foreach ($values as $index => $value) {
            if (in_array($index, $this->encryptable)) {
                $values[$index] = Crypt::decrypt($value);
            }
        }

        return $values;
    }
}
